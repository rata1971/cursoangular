import { Component, OnInit, HostBinding, Input, Output, EventEmitter } from '@angular/core';
import { Book } from '../../model/book';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit {
  @Input() libro: Book;
  @HostBinding('attr.class') cssClass = 'col-md-4';

  constructor() { 
  }

  ngOnInit(): void {
    console.log(this.libro);
  }

}

