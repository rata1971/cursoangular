import { Component, OnInit } from '@angular/core';
import { BookServiceService } from 'src/app/book-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

  constructor(
    private router: Router, 
    private bookService: BookServiceService) { 
  }

  ngOnInit(): void {
  }


  agregar(isdb: number, name:string, detail: string, pages: number) {
    console.log(name);
    this.bookService.addBook(isdb, name, detail, pages);
    this.router.navigate(['../list/']);
  }
}
