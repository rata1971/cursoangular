import { Component, OnInit } from '@angular/core';
import { Book } from '../../model/book';
import { Router } from '@angular/router';
import { BookServiceService } from 'src/app/book-service.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  libros: Book[];

  constructor(private router: Router, private bookService: BookServiceService) {
  }

  ngOnInit(): void {
    this.libros = this.bookService.getAllBooks();
  }

  createNewBook() {
//    this.router.navigateByUrl('/edit/');
    console.log('Cambiando a EDIT');
    this.router.navigate(['../edit/']);
  }

}
