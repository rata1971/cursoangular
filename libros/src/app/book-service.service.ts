import { Injectable } from '@angular/core';
import { Book } from './model/book';

@Injectable({
  providedIn: 'root'
})
export class BookServiceService {
  libros: Book[];

  constructor() {
    this.libros = [];
    const buk = new Book();
    buk.name = "Libro1";
    buk.detail = "Descripcion";
    buk.isdb = 13;
    buk.pages = 15;
    this.libros.push(buk);

    const buk2 = new Book();
    buk2.name = "Largo";
    buk2.detail = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ultrices dictum auctor. Sed feugiat, libero at volutpat bibendum, libero elit";
    buk2.isdb = 1231276;
    buk2.pages = 1235;
    this.libros.push(buk2);

    console.log(this.libros);

   }

   addBook(isdb: number, name: string, detail: string, pages: number) {
    const nb = new Book();
    nb.name = name;
    nb.detail = detail;
    nb.isdb = isdb;
    nb.pages = pages;
    this.libros.push(nb);
   }


   getAllBooks() {
     return this.libros;
   }
}
